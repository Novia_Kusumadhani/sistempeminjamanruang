<?php

namespace App\Http\Controllers;

use App\Models\Peminjam;
use Illuminate\Http\Request;

class berandaController extends Controller
{
    public function index()
    {
        if (auth()->user()->level == 'Admin') {
            return view('beranda.index', [
                'title' => 'Beranda',
                'peminjaman' => Peminjam::orderBy('created_at', 'desc')->where('sts_pinjam', '=', null)->orWhereIn('sts_pinjam', [1])->paginate()
            ]);
        } else {
            return view('beranda.index', [
                'title' => 'Beranda',
                'peminjaman' => Peminjam::where('no_induk', auth()->user()->nim)->orderBy('created_at', 'desc')->where('sts_pinjam', '=', null)->orWhereIn('sts_pinjam', [1])->paginate()
            ]);
        }
    }
}

@extends('layouts.index')

@section('body')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Archive Peminjaman</h1>
                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="card">
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr class="text-center">
                                @if (auth()->user()->level != 'Admin' || auth()->user()->level != 'Moderator')
                                    <th>Status Peminjam</th>
                                    <th>Nama Peminjam</th>
                                    <th>Program Studi</th>
                                    <th>Nama Kegiatan</th>
                                    <th>Nomor Handphone</th>
                                @endif
                                <th>Nama Ruang</th>
                                <th>Tanggal Kegiatan</th>
                                <th>Waktu</th>
                                <!-- @if (auth()->user()->level == 'Admin' || auth()->user()->level == 'Moderator')
                                    <th>Aksi</th>
                                @endif -->

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($peminjaman as $item)
                                <tr class="text-center">
                                    @if (auth()->user()->level != 'Admin' || auth()->user()->level != 'Moderator')
                                        <td>{{ $item->level }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->prodi }}</td>
                                        <td>{{ $item->kegiatan }}</td>
                                        <td>{{ $item->nohp }}</td>
                                    @endif
                                    <td>{{ $item->get_ruang->nm_ruang }}</td>
                                    <td>{{ $item->tgl_kegiatan }}</td>
                                    <td>{{ $item->get_jadwal->waktu }}</td>

                                    <!-- @if (auth()->user()->level == 'Admin' || (auth()->user()->level == 'Moderator' && $item->tgl_kegiatan >= $today))
                                        <td>
                                            <a href="/edit-peminjaman/{{ $item->id }}"
                                                class="btn btn-warning btn-xs"><i class="fas fa-pencil-alt"></i></a>
                                            <a href="/delete-peminjaman/{{ $item->id }}"
                                                class="btn btn-danger btn-xs"><i class="fas fa-trash"></i></a>
                                        </td>
                                    @endif -->
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#example1")
                .DataTable({
                    responsive: true,
                    lengthChange: false,
                    autoWidth: false,
                    buttons: [
                        {
                            extend: 'excel',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdf',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'print',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        'colvis'
                    ],
                })
                .buttons()
                .container()
                .appendTo("#example1_wrapper .col-md-6:eq(0)");
        });
    </script>
@endsection
